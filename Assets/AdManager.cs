using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour
{
    [SerializeField] string _androidAdUnitId = "4630386";
    [SerializeField] string _iOsAdUnitId = "4630387";
    string _adUnitId;

    void Awake()
    {
        // Get the Ad Unit ID for the current platform:
        _adUnitId = (Application.platform == RuntimePlatform.IPhonePlayer) ? _iOsAdUnitId : _androidAdUnitId;
    }
    // Start is called before the first frame update
    void Start()
    {
        Advertisement.Initialize(_adUnitId);
    }

    public void ShowBanner()
    {
        if(Advertisement.isInitialized && Advertisement.isSupported && (!Advertisement.isShowing))
        {
            Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
            if(Application.platform == RuntimePlatform.IPhonePlayer)
            {
                Advertisement.Banner.Show("Banner_iOS");
            }
            else
            {
                Advertisement.Banner.Show("Banner_Android");
            }
            Debug.Log("Show ads");
        }
    }

    void Update()
    {
        ShowBanner();
    }
}
