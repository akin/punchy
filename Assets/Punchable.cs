﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Punchable : MonoBehaviour
{
    public AudioClip[] painAudio;
    public void Punch(Vector3 force, Vector3 position)
    {
        var rigidbody = GetComponent<Rigidbody>();
        if(rigidbody)
        {
            rigidbody.AddForceAtPosition(force, position, ForceMode.Impulse);
        }
        if(painAudio.Length > 0)
        {
            int index = Random.Range(0, painAudio.Length);
            var audioSource = GetComponent<AudioSource>();
            if(audioSource != null)
            {
                audioSource.PlayOneShot(painAudio[index], 1.0f);
            }
        }
    }
}
