﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsShooter : MonoBehaviour
{
    public float CooldownSeconds = 1.0f;
    public float PunchForce = 10.0f;
    private float timer = 0.0f;
    // Update is called once per frame
    void Update()
    {
        if(timer > 0.0f)
        {
            timer -= Time.deltaTime;
            if(timer < 0.0f)
            {
                timer = 0.0f;
            }
        }

        if(timer == 0.0f && Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit, 100)) 
            {
                var punchable = hit.transform.GetComponent<Punchable>();
                if(punchable)
                {
                    punchable.Punch(Camera.main.transform.forward * PunchForce, hit.point);
                    timer = 1.0f;
                }
            }
        }
    }
}
